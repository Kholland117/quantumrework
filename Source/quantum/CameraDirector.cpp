// Fill out your copyright notice in the Description page of Project Settings.


#include "CameraDirector.h"
#include "Kismet/GameplayStatics.h"


// Sets default values
ACameraDirector::ACameraDirector()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACameraDirector::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACameraDirector::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	CHAR Key = 'b';
	const float TimeBetweenCameraChang = 2.0f;
	const float SmoothBlendTime = .075f;
	TimeToNextCameraChange -= DeltaTime;
	
	if (TimeToNextCameraChange <= 0)
	{
		TimeToNextCameraChange += TimeBetweenCameraChang;


		//Find the actor the handles Camera controls from the local player

		APlayerController* OurPlayerController = UGameplayStatics::GetPlayerController(this, 0);

		//If OurPlayerController is found
		if (OurPlayerController) {
			//If cameraTwo Exists and out player camera is cameraOne then go ahead and Blend to CameraTwo
			if (CameraTwo && (OurPlayerController->GetViewTarget() == CameraOne)) {
				OurPlayerController->SetViewTargetWithBlend(CameraTwo, SmoothBlendTime);

			}
			//OtherWise Cut To CameraOne.
			else if (CameraOne) {
				OurPlayerController->SetViewTarget(CameraOne);
			}
		}
	}
}

