// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "QuantumSaveGame.generated.h"

/**
 * 
 */
UCLASS()
class QUANTUM_API UQuantumSaveGame : public USaveGame
{
	GENERATED_BODY()
	
public:
	UQuantumSaveGame();

	UPROPERTY(EditAnywhere)
	FVector PlayerLocation;

};
