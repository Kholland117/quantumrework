// Fill out your copyright notice in the Description page of Project Settings.


#include "AddRadialImpulse.h"
#include "DrawDebugHelpers.h"
#include "Runtime/Engine/Public/WorldCollision.h"	
#include "Kismet/GameplayStatics.h"
#include "Components/MeshComponent.h"
#include "Engine.h"

// Sets default values
AAddRadialImpulse::AAddRadialImpulse()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AAddRadialImpulse::BeginPlay()
{
	Super::BeginPlay();
	// Create TArray For Hit Results
	TArray<FHitResult> OutHits;


	

	FVector MyLocation = GetActorLocation();
	//Start and End Locations
	

	FCollisionShape MyColSphere = FCollisionShape::MakeSphere(500.f);
	//Draw Debug
	DrawDebugSphere(GetWorld(), MyLocation, MyColSphere.GetSphereRadius(), 50, FColor::Cyan, true);

	//Check If Something got Hit in the Sweep
	bool isHit = GetWorld()->SweepMultiByChannel(OutHits, MyLocation, MyLocation, FQuat::Identity, ECC_WorldStatic, MyColSphere);

	if (isHit)
	{
		for (auto& Hit : OutHits)
		{
			UStaticMeshComponent* MeshComp = Cast<UStaticMeshComponent>((Hit.GetActor())->GetRootComponent());

			if (MeshComp)
			{
				MeshComp->AddRadialImpulse(MyLocation, 500.0f, 200000.0f, ERadialImpulseFalloff::RIF_Constant, true);
			}
		}
	}
}

// Called every frame
void AAddRadialImpulse::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

