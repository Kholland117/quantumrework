// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "quantumCharacter.h"
#include "GameFramework/Actor.h"
#include "Medkit.generated.h"

UCLASS()
class QUANTUM_API AMedkit : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AMedkit();



public:	
	UFUNCTION()
		void OnOverlap(AActor* MyOverlappedActor, AActor* OtherActor);

	UPROPERTY(EditAnywhere)
		AquantumCharacter* MyCharacter;




};
