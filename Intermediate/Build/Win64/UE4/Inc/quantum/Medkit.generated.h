// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
#ifdef QUANTUM_Medkit_generated_h
#error "Medkit.generated.h already included, missing '#pragma once' in Medkit.h"
#endif
#define QUANTUM_Medkit_generated_h

#define quantum_Source_quantum_Medkit_h_13_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnOverlap) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_MyOverlappedActor); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnOverlap(Z_Param_MyOverlappedActor,Z_Param_OtherActor); \
		P_NATIVE_END; \
	}


#define quantum_Source_quantum_Medkit_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnOverlap) \
	{ \
		P_GET_OBJECT(AActor,Z_Param_MyOverlappedActor); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnOverlap(Z_Param_MyOverlappedActor,Z_Param_OtherActor); \
		P_NATIVE_END; \
	}


#define quantum_Source_quantum_Medkit_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMedkit(); \
	friend struct Z_Construct_UClass_AMedkit_Statics; \
public: \
	DECLARE_CLASS(AMedkit, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/quantum"), NO_API) \
	DECLARE_SERIALIZER(AMedkit)


#define quantum_Source_quantum_Medkit_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAMedkit(); \
	friend struct Z_Construct_UClass_AMedkit_Statics; \
public: \
	DECLARE_CLASS(AMedkit, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/quantum"), NO_API) \
	DECLARE_SERIALIZER(AMedkit)


#define quantum_Source_quantum_Medkit_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMedkit(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMedkit) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMedkit); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMedkit); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMedkit(AMedkit&&); \
	NO_API AMedkit(const AMedkit&); \
public:


#define quantum_Source_quantum_Medkit_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMedkit(AMedkit&&); \
	NO_API AMedkit(const AMedkit&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMedkit); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMedkit); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMedkit)


#define quantum_Source_quantum_Medkit_h_13_PRIVATE_PROPERTY_OFFSET
#define quantum_Source_quantum_Medkit_h_10_PROLOG
#define quantum_Source_quantum_Medkit_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	quantum_Source_quantum_Medkit_h_13_PRIVATE_PROPERTY_OFFSET \
	quantum_Source_quantum_Medkit_h_13_RPC_WRAPPERS \
	quantum_Source_quantum_Medkit_h_13_INCLASS \
	quantum_Source_quantum_Medkit_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define quantum_Source_quantum_Medkit_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	quantum_Source_quantum_Medkit_h_13_PRIVATE_PROPERTY_OFFSET \
	quantum_Source_quantum_Medkit_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	quantum_Source_quantum_Medkit_h_13_INCLASS_NO_PURE_DECLS \
	quantum_Source_quantum_Medkit_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> QUANTUM_API UClass* StaticClass<class AMedkit>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID quantum_Source_quantum_Medkit_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
